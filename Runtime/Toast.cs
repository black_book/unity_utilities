
using UnityEngine;
namespace BlackBook.Unity_Utilities
{
    /// <summary>
    /// /// Cross-platform class for displaying a toast message on the screen.
    /// - Mattias Tronslien 
    /// </summary>
    public static class Toast
    {
        /// <summary>
        /// Display a message to the user and posts it to the application log
        /// </summary>
        /// <param name="message">The message to display</param>
        /// <param name="DevOnly">if true, this message will only be shown in the unity editor or on development builds</param>
        public static void Show(string message, bool DevOnly = true)
        {
            Debug.Log(message);
#if UNITY_EDITOR
            UnityEditor.EditorUtility.DisplayDialog("Toast", message, "Continue");
#endif
#if !DEVELOPMENT_BUILD
            if (DevOnly) return;
#endif
#if UNITY_ANDROID
            AndroidJavaClass unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
            AndroidJavaObject unityActivity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity");

            if (unityActivity != null)
            {
                AndroidJavaClass toastClass = new AndroidJavaClass("android.widget.Toast");
                unityActivity.Call("runOnUiThread", new AndroidJavaRunnable(() =>
                {
                    AndroidJavaObject toastObject = toastClass.CallStatic<AndroidJavaObject>("makeText", unityActivity, message, 0);
                    toastObject.Call("show");
                }));
            }
#endif
#if UNITY_IOS
            //EasyUI.Toast.Toast.Show(message, 3f);
#endif

        }
    }

}