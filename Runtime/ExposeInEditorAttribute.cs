﻿/****************************************************/
//                Expose In Editor                  //
//                   Version 0.47                   //
//                by Thomas Beswick                 //
/****************************************************/
//              Now supports:                       //
//                  Any and all parameters          //
/****************************************************/


using System;

namespace Sabotender
{
    [AttributeUsage(AttributeTargets.Method)]
    public class ExposeInEditorAttribute : Attribute
    {
        public bool ShowAtTop = false;
        public bool ShowWarning = false;
        public bool AllowInEditor = true;
        public bool AllowWhilePlaying = true;
        public string ButtonNameOverride = "";
        public ConsoleColor ButtonColorOverride = ConsoleColor.White;
    }
}

